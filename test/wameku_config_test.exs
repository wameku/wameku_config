defmodule WamekuConfigTest do
  use ExUnit.Case

  test "can read config file" do
    config_path = Path.join([System.cwd, "test", "support", "sample.config"])
    config = WamekuConfig.read!(config_path)
    assert Keyword.has_key?(config, :system)
  end
end
