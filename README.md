# WamekuConfig

**TODO: Add description**

## Installation

If [available in Hex](https://hex.pm/docs/publish), the package can be installed as:

  1. Add wameku_config to your list of dependencies in `mix.exs`:

        def deps do
          [{:wameku_config, "~> 0.0.1"}]
        end

  2. Ensure wameku_config is started before your application:

        def application do
          [applications: [:wameku_config]]
        end

